#pragma once

#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

namespace common {

std::vector<std::string> read_file(const std::string &path) {

  std::string line;
  std::ifstream input(path);
  std::vector<std::string> file;

  if (input.is_open()) {
    while (std::getline(input, line)) {
      file.emplace_back(line);
    }
    input.close();
  } else {
    std::cerr << "unable to open file" << std::endl;
  }

  return file;
}

std::vector<std::string> split(const std::string &s, char seperator) {
  std::vector<std::string> output;

  std::string::size_type prev_pos = 0, pos = 0;

  while ((pos = s.find(seperator, pos)) != std::string::npos) {
    std::string substring(s.substr(prev_pos, pos - prev_pos));

    output.push_back(substring);

    prev_pos = ++pos;
  }

  output.push_back(s.substr(prev_pos, pos - prev_pos)); // Last word

  return output;
}
} // namespace common
