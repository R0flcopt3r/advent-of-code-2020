#include <iostream>
#include <stdlib.h>
#include <vector>

// Advent of code 2020 Day 1
//
// This solves part one and two

std::vector<std::vector<int>> find_three_sum(std::vector<int> nums, int sum);
std::vector<std::vector<int>> find_two_sum(std::vector<int> nums, int sum);

int main(int argc, char **argv) {

  std::vector<int> nums;

  if (argc > 0) {
    for (int i = 1; i < argc; i++) {
      nums.push_back(atoi(argv[i]));
    }
  }

  std::vector<std::vector<int>> results;

  results = find_two_sum(nums, 2020);
  for (auto &r : results) {
    std::cout << "part 1: " << r[0] * r[1] << std::endl;
  }

  results = find_three_sum(nums, 2020);
  for (auto &r : results) {
    std::cout << "part 2: " << r[0] * r[1] * r[2] << std::endl;
  }
}

std::vector<std::vector<int>> find_three_sum(std::vector<int> nums, int sum) {
  std::vector<std::vector<int>> result;
  for (int i = 0; i < nums.size(); i++) {
    int k = nums.back();
    nums.pop_back();

    std::vector<int> num = nums;
    for (int i = 0; i < num.size(); i++) {
      int v = num.back();
      num.pop_back();

      for (auto &n : num) {
        if (n + k + v == sum) {
          result.push_back({n, k, v});
        }
      }
    }
  }
  return result;
}

std::vector<std::vector<int>> find_two_sum(std::vector<int> nums, int sum) {
  std::vector<std::vector<int>> result;
  for (int i = 0; i < nums.size(); i++) {
    int k = nums.back();
    nums.pop_back();
    for (auto &n : nums) {
      if (k + n == sum) {
        result.push_back({k, n});
      }
    }
  }
  return result;
}
