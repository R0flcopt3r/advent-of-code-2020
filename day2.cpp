#include <algorithm>

#include "common.hpp"

bool part1(char c, std::string pwd, std::vector<int> min_max);
bool part2(char c, std::string pwd, std::vector<int> locations);

int main(int argc, char **argv) {

  if (argc <= 1) {
    std::cerr << "provide file" << std::endl;
    exit(1);
  }

  auto input = common::read_file(argv[1]);

  std::vector<std::vector<std::string>> words;
  int part1_valid = 0;
  int part2_valid = 0;

  for (auto &w : input) {
    words.push_back(common::split(w, ' '));
  }

  for (auto &l : words) {

    std::vector<int> min_max;
    for (auto c : common::split(l[0], '-')) {
      min_max.emplace_back(std::atoi(c.c_str()));
    }

    char c = l[1].front();

    if (part1(c, l[2], min_max)) {
      part1_valid++;
    }
    if (part2(c, l[2], min_max)) {
      part2_valid++;
    }
  }

  std::cout << "part1: " << part1_valid << std::endl;
  std::cout << "part2: " << part2_valid << std::endl;
}

bool part1(char c, std::string pwd, std::vector<int> min_max) {
  auto count = std::count(pwd.begin(), pwd.end(), c);
  if (count >= min_max.front() && count <= min_max.back()) {
    return true;
  }
  return false;
}

bool part2(char c, std::string pwd, std::vector<int> locations) {

  if (pwd[locations.front() - 1] == pwd[locations.back() - 1]) {
    return false;
  }

  if (pwd[locations.front() - 1] == c) {
    return true;
  }

  if (pwd[locations.back() - 1] == c) {
    return true;
  }

  return false;
}
