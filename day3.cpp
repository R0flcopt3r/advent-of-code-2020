#include "common.hpp"

int main(int argc, char **argv) {

  if (argc <= 1) {
    std::cerr << "provide file" << std::endl;
    exit(1);
  }

  auto input = common::read_file(argv[1]);

  std::vector<std::vector<int>> directions = {
      {1, 1, 0}, {3, 1, 0}, {5, 1, 0}, {7, 1, 0}, {1, 2, 0},
  };

  for (auto &i : directions) {
    int right = i[0];
    int down = i[1];
    int x = 0;
    int y = 0;

    while (y < input.size()) {
      if (input[y][x] == '#') {
        i[2]++;
      }
      x += right;
      y += down;
      if (x >= input[y].size())
        x = x - input[y].size();
    }
  }

  uint64_t result = 1;
  for (auto &i : directions) {
    std::cout << i[2] << std::endl;
    result *= i[2];
  }
  std::cout << result << std::endl;
}
