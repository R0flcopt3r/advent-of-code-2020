#include "common.hpp"
#include <algorithm>
#include <cctype>
#include <regex>
#include <stdlib.h>

struct passport {
  std::pair<int, int> byr = {1920, 2002};
  std::pair<int, int> iyr = {2010, 2020};
  std::pair<int, int> eyr = {2020, 2030};
};

std::vector<std::vector<std::string>> serialize(std::vector<std::string> input);

int main(int argc, char **argv) {

  if (argc <= 1) {
    std::cerr << "provide file" << std::endl;
    exit(1);
  }
  auto input = common::read_file(argv[1]);

  std::vector<std::vector<std::string>> santized = serialize(input);

  /* Part one! */
  std::vector<std::pair<std::string, bool>> required_field = {
      {"byr", false}, {"iyr", false}, {"eyr", false}, {"hgt", false},
      {"hcl", false}, {"ecl", false}, {"pid", false},

  };

  std::unordered_map<std::string, std::regex> checks = {
      {"byr", std::regex("(19[2-9][0-9]|200[1-2]|2000)")}, // 1920-2002
      {"iyr", std::regex("(201[0-9]|2020)")},              // 2010-2020
      {"eyr", std::regex("(202[0-9]|2030)")},              // 2020-2030
      {"hgt",
       std::regex(
           "((1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in)")}, // 150-190cm
                                                               // 59-76in
      {"hcl", std::regex("#([0-9]|[a-f]){6}")},                // hex color
      {"ecl", std::regex("(amb|blu|brn|gry|grn|hzl|oth)")},    // color
      {"pid", std::regex("[0-9]{9}")}, // 9 digit leading zero
      {"cid", std::regex(".*")},       // anything
  };

  int valid_passports = 0;
  for (auto &passport : santized) {
    int valid = 0;
    for (auto &w : passport) {
      auto field = common::split(w, ':').front();
      for (auto &f : required_field) {
        if (!f.second && f.first == field) {
          f.second = true;
        }
      }
    }
    for (auto &f : required_field) {
      if (f.second) {
        valid++;
        f.second = false;
      }
    }
    if (valid >= required_field.size()) {
      valid_passports++;
    }
  }

  std::cout << "part 1: " << valid_passports << std::endl;

  /* Part 2 */

  valid_passports = 0;

  for (auto &passport : santized) {
    int valid = 0;
    for (auto &w : passport) {
      auto field = common::split(w, ':');
      if (!std::regex_match(field.back(), checks[field.front()])) {
        break;
      }
      if (field.front() != "cid") {
        valid++;
      }
    }
    if (valid >= required_field.size()) {
      valid_passports++;
    }
  }

  std::cout << "Part 2: " << valid_passports << std::endl;
}

std::vector<std::vector<std::string>>
serialize(std::vector<std::string> input) {
  std::vector<std::vector<std::string>> sanitized;
  std::vector<std::string> line;
  for (auto &l : input) {
    if (!l.empty()) {
      for (auto &w : common::split(l, ' ')) {

        w.erase(std::remove_if(w.begin(), w.end(), ::isspace), w.end());

        line.push_back(w);
      }
    } else {
      sanitized.push_back(line);
      line.clear();
    }
  }
  sanitized.push_back(line);

  return sanitized;
}
