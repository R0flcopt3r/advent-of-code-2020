#include "common.hpp"
#include <algorithm>

int main(int argc, char **argv) {

  if (argc <= 1) {
    std::cerr << "provide file" << std::endl;
    exit(1);
  }
  auto input = common::read_file(argv[1]);

  std::vector<int> boarding_passes;

  for (auto &seat : input) {

    int back = 127;
    int front = 0;

    int right = 7;
    int left = 0;

    char last_x;
    char last_y;

    int val_y = 128;
    int val_x = 8;
    for (auto &c : seat) {

      if (c == 'B') {
        val_y /= 2;
        front += val_y;
        last_y = c;
      }
      if (c == 'F') {
        val_y /= 2;
        back -= val_y;
        last_y = c;
      }

      if (c == 'L') {
        val_x /= 2;
        right -= val_x;
        last_x = c;
      }
      if (c == 'R') {
        val_x /= 2;
        left += val_x;
        last_x = c;
      }
    }

    int row;
    int column;

    if (last_y == 'B') {
      row = back;
    } else {
      row = front;
    }
    if (last_x == 'R') {
      column = right;
    } else {
      column = left;
    }

    boarding_passes.emplace_back(row * 8 + column);
  }

  std::sort(boarding_passes.begin(), boarding_passes.end());
  std::cout << boarding_passes.back() << std::endl;

  for (int i = 0; i < boarding_passes.size() - 1; i++) {
    if (boarding_passes[i] + 1 != boarding_passes[i + 1]) {
      std::cout << boarding_passes[i] + 1 << std::endl;
      break;
    }
  }
}
